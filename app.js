'use strict';

var
    express = ('express'),
    app = express(),
    http = require('http').createServer(app),
    io = require('socket.io')(http),
    port = process.env.PORT || 3001;

var publicDir = express.static(__dirname + '/public');

app
    .use(publicDir)
    .get('/', function (req, res) {
        res.sendFile(publicDir + '/index.html');
})


http.listen(port,function () {
    console.log('Iniciando Express y Socket IO en el puerto : %d', port)
});

io.on('connection', function (socket) {
    socket.broadcast.emit('new user', {message: 'Ha entrado un usuario al chat.'});
    socket.on('new message', function (message) {
        io.emit('user say', message);
    })
});

module.exports = app;
